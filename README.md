# ECHOedLinks

## Echo Links, right at your fingertips!

Don't you hate having to click on course blocks for those recorded lectures?

Well, those days are over!

### Introducing ECHOedLinks

<p align="center">
    <img src="img/img_1.png">
</div>

## BUT WAIT, THERE'S MORE!!!!!!!!

You also get this handy extension accessible at the top of the screen that has all of your course ECHO links!

<p align="center">
    <img src="img/img_3.png">
</div>

## BUT WAITTTTTTTT, THERE'S STILLL MOOOOOREEEE

It comes it dark theme as well 
<p align="center">
    <img src="img/img_2.png">
</div>

