var editingChange = false;
var button = document.querySelector("#refreshButton");
//This click listener is for refreshing the course list
button.addEventListener("click", function() {
    //Clears error message box
    document.querySelector("#errorMessage").textContent = "";

    //If active tab is the main learn page, clear the cache and call the main.js file to remake the course list.
    chrome.tabs.query({active: true, currentWindow: true}, function(tabList){
        var tab = tabList[0];
        if (String(tab.url).startsWith("https://learn.canterbury.ac.nz/my/")) {
            chrome.storage.sync.clear(function() {
                chrome.tabs.sendMessage(tab.id,{ msg: "refreshLinksOnPage" });
            });
        } else {
            //If the user is not on the main learn page, throw this error
            document.querySelector("#errorMessage").textContent = "Please go to the main LEARN page to refresh your course information";
            openLearn();
        }
    });
});
//Toggle editing mode when click edit button and button is not hidden
var editButton = document.querySelector("#editLinks");
editButton.addEventListener("click", function() {
    if (!editButton.classList.contains("hideEditButton")){
        toggleEditingMode();
    }
    
});
//Leave editing mode when done button clicked. If user is on main LEARN page, call main.js to update the page's ECHO Links
var doneButton = document.querySelector("#doneButton");
doneButton.addEventListener("click", function() {
    toggleEditingMode();
    chrome.tabs.query({active: true, currentWindow: true}, function(tabList) {
        var tab = tabList[0];
        if (String(tab.url).startsWith("https://learn.canterbury.ac.nz/my/")) {
            chrome.tabs.sendMessage(tab.id,{ msg: "refreshLinksOnPage" });
        }
    });
});
//Turns on and off editing mode, switches the refresh button and done button, toggles visibility of link checkboxes,
// toggles visibility of hidden links and disables/enables links (all links are disabled in editing mode)
function toggleEditingMode() {
    editButton.classList.toggle("hideEditButton");
    const displayNoneItems = ["#refreshButton", "#doneButton"];
    for (let key of displayNoneItems){
        document.querySelector(key).classList.toggle("displayNone");
    }
    var toggleLinks = document.querySelectorAll(".toggleLink");
    for (let link of toggleLinks) {
        link.classList.toggle("displayNone");
    }
    var echoLinks = document.querySelectorAll(".echoLinkContainer");
    for (let link of echoLinks) {
        if (link.classList.contains("hiddenLink")) link.classList.toggle("reducedTransparency");
        link.firstChild.classList.toggle("disablePointer");
    }
}
//Opens LEARN page
function openLearn() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabList) {
        var tab = tabList[0];
        if (String(tab.url).startsWith("https://learn.canterbury.ac.nz/my/")) {
            chrome.tabs.sendMessage(tab.id,{ msg: "refreshLinksOnPage" }, function() {
                document.querySelector("#errorMessage").textContent = "";
            });
        } else {
            chrome.storage.sync.clear(function() {
                chrome.tabs.create({url: "https://learn.canterbury.ac.nz/my"}, function() {
                    document.querySelector("#errorMessage").textContent = "";
                });
            });
        }
        
    });
}
//Checks cache for course data and populates the extension page with it
function getCourseData() {
    chrome.storage.sync.get(null, function (data) {
        if (data != undefined) {
            var courseInfo = data["courses"];
            var versionNum = data["version"];
            var isSameVersion = versionNum == chrome.runtime.getManifest().version;
            if (courseInfo != undefined && Object.keys(courseInfo).length > 0 && isSameVersion) {
                populateExtensionPage(courseInfo);
            } else {
                openLearn();
            }
        } else {
            openLearn();
        }
    });
}
//If link clicked, get href of button clicked and create a tab from that link
// If link checkbox clicked, toggle the activated state of the checkbox and the hidden status of the link
document.addEventListener("click", function(e) {
    if (e.target.classList[1] == "linkButton") {
        chrome.tabs.create({
            url: e.target.getAttribute("data-href")
        });
    } else if (e.target.classList.contains("toggleLink")) {
        e.target.firstChild.classList.toggle("linkActivated");
        changeHiddenStatusLink(e.target);
    }
});
//Get link corresponding to the button clicked. Get link info from cache and toggle hidden bool. Then toggle hidden css on extension popup.
function changeHiddenStatusLink(button) {
    var link = button.dataset.link;
    var linkId = button.dataset.elem;
    chrome.storage.sync.get(["courses"], function(results) {
        var courseList = results["courses"];
        courseList[link][2] = !courseList[link][2];
        editingChange = true;
        chrome.storage.sync.set({"courses": courseList}, function() {
            var linkElem = document.querySelector("#"+linkId);
            linkElem.classList.toggle("hiddenLink");
            linkElem.classList.toggle("reducedTransparency");
        });
    });
}
//Uses course data to add links to each course to the extension page and also clears the error message.
function populateExtensionPage(links) {
    echoHTML = "";
    var i = 0;
    for (let key of Object.keys(links)) {
        
        var courseName = links[key][0];
        var learnPage = key;
        var echoLink = links[key][1];
        isVisible = links[key][2];
        var hiddenLink = "";
        var linkActivated = "";
        if (isVisible == false) {
            hiddenLink = "hiddenLink";   
        } else {
            linkActivated = "linkActivated";
        }
        
        echoHTML += '<div id="link'+i+'" class="echoLinkContainer '+hiddenLink+'">';
        echoHTML += '<div class="echoLink">';
        echoHTML += '<div class="courseName disablePointer">'+courseName+'</div>';
        echoHTML += '<img class="ucIcons icons disablePointer" src="img/uc.svg" alt="LEARN">';

        echoHTML += '<button data-href="'+learnPage+'" class="learnButton linkButton">⠀</button><button data-href="'+echoLink+'" class="echoButton linkButton">⠀</button>';

        echoHTML += '<img src="img/echo.svg" alt="ECHO360" class="echoIcons icons disablePointer">';
        echoHTML += '<div class="backgroundLearnColour backgroundColour disablePointer"></div>';
        echoHTML += '<div class="backgroundEchoColour backgroundColour disablePointer"></div>';
        echoHTML += '</div>';
        echoHTML += '<button data-link="'+key+'" data-elem="link'+i+'" class="toggleLink displayNone"><div class="toggleLinkInner disablePointer '+linkActivated+'">✓</div></button>';
        echoHTML += '</div>';
        i++;
    }
    //Show edit button when links are available
    if (editButton.classList.contains("hideEditButton")) editButton.classList.remove("hideEditButton");
    var linkSection = document.querySelector('#linkSection');
    linkSection.innerHTML = echoHTML;

}
//When chrome storage changes, unless the changes come from editing mode
document.querySelector("#errorMessage").textContent = "";
chrome.storage.onChanged.addListener(function (changes,_) {
    if (changes.hasOwnProperty("courses") && changes["courses"]["newValue"] != undefined && editingChange == false) {
        getCourseData();
    } else {
        editingChange = false;
    }
});
getCourseData();