function getEchoLinks() {
    var echoBaseLink = "https://learn.canterbury.ac.nz/blocks/alp_player/launch.php?";
    // This gets links from the dropdown at the top of the page
    var dropDownTarget = ".nav-item>.dropdown-menu";

    //If course links have been cached, just use that (force flush cache if cache made in previous version of extension)
    chrome.storage.sync.get(null, function (data) {
        var courseInfo = data["courses"];
        var versionNum = data['version'];
        if (versionNum == chrome.runtime.getManifest().version && data.hasOwnProperty("courses") 
            && Object.keys(courseInfo).length > 0 ) {
            populateLEARNPage(courseInfo);
        } else {
            chrome.storage.sync.clear(function () {
                refreshCache(echoBaseLink, dropDownTarget);
            });
            
        }
    });
}
function refreshCache(echoBaseLink, dropDownTarget) {
    var allCourses  = document.querySelectorAll(dropDownTarget)[0];
    var echoLinks = {};
    // For each course, get link (href). If course has an ID (not the dashboard) and the course either is from the current year
    // or is a summer school/full year/cross year course
    // use the base link and the course ID as the key for the object and the course name as the value 
    // This is because URLs are guaranteed to be unique
    for (let i = 0; i < allCourses.children.length; i++) {
        let courseLink = allCourses.children[i].getAttribute("href");
        
        let idPosition = courseLink.indexOf("id=");
        let courseName = allCourses.children[i].getAttribute("title");
        let yearNum = new Date().getFullYear();
        let year = String(yearNum);
        let summerSchool = String(--yearNum);
        let regex = RegExp(".*[\-]["+year[2]+"]["+year[3]+"].*$");
        let summerSchoolRegex = RegExp(".*[\-]["+summerSchool[2]+"]["+summerSchool[3]+"]([S][U][0-9])|(([C]|[F])[Y])$");
        if (idPosition != -1 && (regex.test(courseName) || summerSchoolRegex.test(courseName))) {
            let courseId = courseLink.slice(idPosition);
            
            echoLinks[courseLink] = [courseName, echoBaseLink+courseId, true];
        }
    }
    //Insert the course information into the cache
    chrome.storage.sync.set(
        {
            "courses": echoLinks, 
            "version": chrome.runtime.getManifest().version
        }, function() {
            populateLEARNPage(echoLinks);
    });
    


}
function populateLEARNPage(echoLinks) {

    var mainSection = document.querySelector("#region-main");
    console.log(mainSection);
    var doesExist = document.querySelector("#echoLinkSection");
    //If echo links have already been added to learn page, just remove it
    if (doesExist != null) {
        mainSection.removeChild(doesExist);
    }
    var mainDiv = mainSection.children[2];
    
    var echoHTML = '<section role="complementary" data-block="echo_links" aria-labelledby="echo_links-header" \
        id="echoLinkSection" class="card mb-3">';
    echoHTML += '<div id="echoLinkDiv" class="card-body p-3"><h5 class="card-title d-inline" id="echoLinkHeader">Echo Links</h5>';
    echoHTML += '<div id="echoBoxSection" class="card-text content mt-3" style="display: flex;justify-content: center; flex-wrap: wrap;">';
    var svg = '<svg xmlns="http://www.w3.org/2000/svg" width="192px" height="73px" viewBox="0 0 516.377 195.644"><path fill="#0077c0" d="M64.597 195.514C24.92 193.254-4.545 157.273.58 117.343c4.452-34.69 27.167-57.724 57.97-58.786 37.927-1.307 64.396 29.674 61.922 72.478l-.195 3.373H71.224c-43.678 0-49.054.044-49.054.402 0 4.015 3.79 13.592 7.71 19.479 15.659 23.519 49.219 27.828 72.11 9.259l1.636-1.327 5.772 6.387c3.175 3.512 6.006 6.68 6.29 7.04l.518.653-1.708 1.572c-10.757 9.898-26.56 16.736-40.47 17.512-1.527.085-3.492.194-4.365.241-.873.047-3.153-.003-5.066-.112zm33.653-80.883c-5.779-48.633-66.984-48.205-75.697.529l-.177.992H98.43zm100.133 80.864c-58.578-3.014-85.964-75.164-44.332-116.796 27.017-27.018 74.746-26.464 94.003 1.091l.567.812-6.317 6.313-6.317 6.314-1.807-1.705c-30.94-29.186-82.114-3.855-78.658 38.935 3.223 39.9 48.526 58.637 80.2 33.17l1.754-1.41 6.184 6.883c3.401 3.786 6.22 6.98 6.263 7.1.105.292-3.454 3.374-6.612 5.724-13.208 9.83-28.404 14.42-44.928 13.57zm245.362.019c-59.61-3.396-86.456-77.525-42.88-118.4 42.76-40.11 112.694-11.389 115.443 47.411 1.678 35.882-27.726 68.885-63.132 70.86-1.528.085-3.492.194-4.365.241-.873.047-3.153-.003-5.066-.112zm12.707-21.14c56.257-11.235 46.624-98.365-10.552-95.433-57.552 2.951-60.278 88.996-3.042 96.048 2.566.316 10.796-.056 13.594-.615zm.95-15.287c-4.366-3.047-37.276-28.851-37.676-29.541-.852-1.468-.678-1.62 19.23-16.943 19.668-15.137 19.513-15.028 20.325-14.293.715.647.81 59.628.098 60.646-.494.706-1.096.746-1.977.131zm-191.021-62.91V0H287.812V74.903l1.918-1.623c27.029-22.864 67.29-5.803 75.13 28.437V192.469h-22.225v-90.753c-6.44-24.243-42.585-25.91-55.033-1.322V192.469h-21.299zm142.793-44.245c-.99-.149-1.045-.431-.478-2.455l.482-1.72 3.015.081c7.672.206 12.54-3.378 12.54-9.23 0-4.744-3.122-7.323-9.116-7.527l-2.503-.086-.473-1.673c-.26-.92-.473-1.745-.474-1.833 0-.087.868-.233 1.93-.322 5.59-.473 8.376-2.687 8.067-6.41-.385-4.641-6.788-6.522-11.29-3.316-.998.71-.832.777-2.28-.919l-1.256-1.47.623-.662c1.835-1.954 6.73-3.33 9.948-2.797 8.848 1.466 12.137 11.606 5.37 16.553-.441.322-.55.527-.326.613 2.155.825 5.055 4.239 5.896 6.94 3.035 9.75-7.111 18.122-19.675 16.233zm33.978-10.794c-16.887-4.743-9.676-40.08 8.18-40.08.378 0 1.195 2.193 1.416 3.8.018.125-.683.43-1.556.68-5.468 1.56-9.86 5.86-11.906 11.66-.626 1.773-.539 1.834 1.3.91 8.58-4.32 18.746 2.38 18.079 11.915-.525 7.498-8.372 13.121-15.513 11.115zm6.532-4.905c7.778-3.887 4.67-15.41-4.048-15.008-4.9.225-7.522 2.79-7.535 7.369-.019 6.47 5.991 10.433 11.583 7.639zm23.865 4.89c-11.404-2.996-15.386-16.817-7.229-25.094 9.168-9.301 24.855-3.412 25.667 9.636.61 9.829-9.045 17.924-18.438 15.458zm7.973-5.071c10.188-5.02 6.113-20.845-5.182-20.12-11.168.717-13.292 16.879-2.711 20.63 2.264.802 5.677.582 7.893-.51z"/></svg>';
    // For each course, append to the div the standard echo link with the course link and name (if the links are not hidden).
    for (let key of Object.keys(echoLinks)) {
        if (echoLinks[key].length >= 3 && echoLinks[key][2] == true) {
            
            var courseName = echoLinks[key][0];
            var echoLink = echoLinks[key][1];
            echoHTML += '<div class="block_alp_player" style="padding: 20px;"><a target="_blank" style="display: flex;flex-direction: column;align-items: center;" href="'+echoLink+'"> \
                '+svg+' \
                <br>'+courseName+'</a></div>' 
            ;
        }
    }
    echoHTML += "</div></div></section>";

    mainDiv.insertAdjacentHTML('beforebegin',echoHTML);

}
//Guarantees the script won't be accidentally triggered by the extension upon launch
//Use starts with so that if page ends with index.php or an argument e.g. ?lang=en, it will still work on page
if (String(window.location).startsWith("https://learn.canterbury.ac.nz/my/")) {
    getEchoLinks();
}

//Listener callable by popup.js to update echo links on learn page on change
chrome.runtime.onMessage.addListener(function(request, _, _) {
    if (request.msg == "refreshLinksOnPage") getEchoLinks();
});